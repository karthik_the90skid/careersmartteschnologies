<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'careersm_career' );

/** MySQL database username */
define( 'DB_USER', 'careersm_career' );

/** MySQL database password */
define( 'DB_PASSWORD', 'admin@2019' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-Ky&}k@}wifgb-J.u7I~=yt&0ezS-8}|3[XTU5vU1zPXHUA<^Uq}{kA5@]xR2A,W' );
define( 'SECURE_AUTH_KEY',  '52oE!X9rR[%~S<it6 6lI1[y*fBNmp=n}/6+PrE_[+l:w<cg;a_ 9fmir`m;Tpgo' );
define( 'LOGGED_IN_KEY',    '9A+X?RI3X9$L54)AyPnc*Q6>B+mv2!5%(%Rc$+Ek^*f#4jL7j?L:[/bey{D7 SL{' );
define( 'NONCE_KEY',        'C%I#s$V2C!4UQxTY|@i$;_4Gj~XwVX~l&Ih/&7~mh*0U <Iq& J_gV@h{<J,-Y_]' );
define( 'AUTH_SALT',        '>02HitmPxOn|f7^dzps%E2gJU1L)VhW.sM1/u)HEw@PRH6`t_wCGRZx#H4<X3z)[' );
define( 'SECURE_AUTH_SALT', '2)LkNu&/9fGYFGrvNg011]A~3vQ^hCN}(}w5cj&6obCpBMEc]d6!`7BD!>;=iHp>' );
define( 'LOGGED_IN_SALT',   'Coeqxjhoh@!rO5PAO9?!yP9yoF.c{8M,`XY6nUIL6oL^bKE]{S1_<Qi()hIJ}Sbb' );
define( 'NONCE_SALT',       'wc_BA+f&hu&f$pw#hVi^6XPng}YWn)wY3xR_|>X3x`0rt%t>y3k=MY}5S7RP=-`6' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
